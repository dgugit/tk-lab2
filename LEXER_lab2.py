from enum import Enum


class Lexems(Enum):
    tword = 'ttt'
    pword = 'ppp'
    sword = 'sss'
    digit_8 = '01234567'
    ulOpenTag = '<ul>'
    ulCloseTag = '</ul>'
    olOpenTag = '<ol>'
    olCloseTag = '</ol>'
    liOpenTag = '<li>'
    liCloseTag = '</li>'


class Lexer:
    lines = []
    line_number = 0
    head_position = 0

    def cleanFile(self):
        open('output.txt', 'w').close()

    def ReadFromFile(self):
        with open('input.txt', 'r') as file:
            self.lines = file.readlines()
        print("Inputed text consist of " + str(len(self.lines)) + " sentences:")
        for line in self.lines:
            print(line)

    def WriteLexemToFile(self, lexemtype, lexem):
        with open('output.txt', 'a') as file:
            file.write("<" + lexemtype + ",'" + lexem + "'> ")
        print("<" + lexemtype + ", " + lexem + "> ")

    def WriteError(self, position, type):
        with open('output.txt', 'a') as file:
            file.write("<" "error in sentence #" + str(self.line_number) + " in position " + str(
                position) + " with " + type + " >")
        print("<" "error in sentence #" + str(self.line_number) + " in position " + str(
            position) + " with " + type + " >")
        self.WriteLexemToFile('END', 'new line')
        self.LinesCheck()

    def getSymbolForGetter(self):
        line = self.lines[self.line_number]
        symbol = line[self.head_position]

        if symbol == ' ' or symbol == '\t':
            while symbol == ' ' or symbol == '\t':
                self.head_position = self.head_position + 1
                symbol = line[self.head_position]

        # print("tokener give - `" + symbol + "` " + str(self.head_position))
        self.head_position = self.head_position + 1
        return symbol

    def getSymbol(self):
        line = self.lines[self.line_number]
        symbol = line[self.head_position]
        # print("getter give - `" + symbol + "` " + str(self.head_position))
        self.head_position = self.head_position + 1
        return symbol

    def getSymbolByIndex(self, index):
        line = self.lines[self.line_number]
        symbol = line[index]

    def LinesCheck(self):
        numberOfLines = len(self.lines)
        if self.line_number != (numberOfLines - 1):
            self.line_number = self.line_number + 1
            self.head_position = 0
            self.get_token()
        else:
            self.line_number = len(self.lines)
            return
            # print("--- " + str(self.line_number) + " " + str(numberOfLines))

    def get_token(self):
        symbol = self.getSymbolForGetter()
        if symbol == '\n':
            if self.line_number == self.lines.__len__() - 1:
                print("end")
                return
            else:
                self.WriteLexemToFile('END', 'new line')
                self.LinesCheck()
        if symbol == 'p' or symbol == 't' or symbol == 's':
            self.word(symbol)
        if symbol == '<':
            self.tag(symbol)
        if symbol == "0" or symbol == "1" or symbol == "2" or symbol == "3" or symbol == "4" or symbol == "5" or symbol == "6" or symbol == "7":
            self.digit8(symbol)

    def returnHead(self):
        self.head_position = self.head_position - 1
        # print("head returned to " + str(self.head_position))

    def word(self, preSymbol):
        # print("word lexer")
        flag = 1
        for i in range(2):
            sym = self.getSymbol()
            # print("checking - " + sym)
            if sym != preSymbol:
                flag = 0
                position = self.head_position
        if flag == 0:
            if preSymbol == 'p':
                return self.WriteError(position, "wrong ppp word")
            if preSymbol == 't':
                return self.WriteError(position, "wrong ttt word")
            if preSymbol == 's':
                return self.WriteError(position, "wrong sss word")
        if flag == 1:
            if preSymbol == 'p':
                self.WriteLexemToFile(Lexems.pword.name, Lexems.pword.value)
                return self.get_token()
            if preSymbol == 't':
                self.WriteLexemToFile(Lexems.tword.name, Lexems.tword.value)
                return self.get_token()
            if preSymbol == 's':
                self.WriteLexemToFile(Lexems.sword.name, Lexems.sword.value)
                return self.get_token()


    def tag(self, preSymbol):
        # print("tag lexer")
        flag = 1
        symbol = self.getSymbol()
        if symbol == '/':
            symbol = self.getSymbol()
            type = 0
            lexemFinder = 0
            for lexem in (Lexems.liCloseTag, Lexems.olCloseTag, Lexems.ulCloseTag):
                lexemFinder = 1
                if symbol != lexem.value[2]:
                    lexemFinder = 0
                    continue
                if self.getSymbol() != lexem.value[3]:
                    lexemFinder = 0
                    continue
                if self.getSymbol() != lexem.value[4]:
                    lexemFinder = 0
                    continue
                if lexemFinder:
                    type = lexem
                    break

            if lexemFinder:
                self.WriteLexemToFile(lexem.name, lexem.value)
                return self.get_token()
            else:
                position = self.head_position
                return self.WriteError(position, "wrong closing tag")
        else:
            type = ''
            lexemFinder = 0

            for lexem in (Lexems.liOpenTag, Lexems.olOpenTag, Lexems.ulOpenTag):
                lexemFinder = 1
                if symbol != lexem.value[1]:
                    lexemFinder = 0
                    continue
                if self.getSymbol() != lexem.value[2]:
                    lexemFinder = 0
                    continue
                if self.getSymbol() != lexem.value[3]:
                    lexemFinder = 0
                    continue
                if lexemFinder:
                    type = lexem
                    break

            # for lexem in (Lexems.liOpenTag, Lexems.olOpenTag, Lexems.ulOpenTag):
            #     if symbol == lexem.value[1]:
            #         if self.getSymbol() == lexem.value[2]:
            #             if self.getSymbol() == lexem.value[3]:
            #                 LexemFinder = 1
            #                 type = lexem
            if lexemFinder:
                self.WriteLexemToFile(lexem.name, lexem.value)
                return self.get_token()
            else:
                position = self.head_position
                return self.WriteError(position, "wrong open tag")


    def digit8(self, preSymbol):
        # print("number lexer")
        if preSymbol == '0':
            self.WriteLexemToFile("8-digit number", "0")
        else:
            symbol = preSymbol
            number = preSymbol
            symbol = self.getSymbol()
            while symbol == "0" or symbol == "1" or symbol == "2" or symbol == "3" or symbol == "4" or symbol == "5" or symbol == "6" or symbol == "7":
                number = number + symbol
                symbol = self.getSymbol()
            self.WriteLexemToFile("8-digit number", number)
            self.returnHead()
        return self.get_token()


Lex = Lexer()
Lex.cleanFile()
Lex.ReadFromFile()
Lex.get_token()
